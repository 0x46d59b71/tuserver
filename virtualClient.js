var
	crypto = require("crypto"),
	fs = require("fs"),
	zlib = require("zlib"),
	url = require("url"),
	path = require("path"),
	net = require("net");

module.exports = {
	//Secondary variables
		players: {
			//Secondary variables
				defaultGuiData: {},
				
				list: {},
			
			//Methods
				New: function(name) {
					var spawnPoint = module.exports.players.findSpawnPoint();
					
					Object.assign(this, 
						new game.realms.gameWorld.objectTypes.player.New(),
						
						{
							name: name,
							
							position: {
								x: spawnPoint.x,
								y: spawnPoint.y
							},
							cameraPosition: {
								x: spawnPoint.x - game.realms.gameWorld.playerOffset.x,
								y: spawnPoint.y - game.realms.gameWorld.playerOffset.y,
							},
							
							currentGuiType: "default",
							guiData: JSON.parse(JSON.stringify(
								module.exports.players.defaultGuiData
							))
						}
					);
				},
				
				findSpawnPoint: function() {
					for (
						var
							x = Math.floor(Math.random() * 9007199254740991) * [
								1,
								-1
							][+(Math.random() < 0.5)],
							y = Math.floor(Math.random() * 9007199254740991) * [
								1,
								-1
							][+(Math.random() < 0.5)];;
					) {
						if (!game.realms.gameWorld.objects[[x, y]]) {
							return {
								x: x,
								y: y
							};
						}
					}
				},
				
				setCurrent: function(player) {
					if (this.list[player]) {
						this.current = player;
						
						game.config.list.gameWorld.playerPosition =
							this.list[player].position;
						game.config.list.gameWorld.cameraPosition =
							this.list[player].cameraPosition;
						
						game.realms.gameWorld.currentGuiType.data =
							this.list[player].guiData[
								this.list[player].currentGuiType
							];
					}
				},
				
				connect: function(player) {
					Object.defineProperty(this.list, player, {
						configurable: true,
						enumerable: true,
						
						get: new Function("",
							"return game.config.list.players[" +
								"\"" + player + "\"" +
							"];"
						),
						set: new Function("value",
							"game.config.list.players[" +
								"\"" + player + "\"" +
							"] = value;"
						)
					});
					
					if (!game.config.list.players[player]) {
						this.list[player] = new this.New(player);
					}
					
					game.realms.gameWorld.objects[[
						this.list[player].position.x,
						this.list[player].position.y
					]] = this.list[player];
					
					console.log(player + " logged in");
					game.realms.gameWorld.guiTypes.default.writeToChat("",
						player + game.currentLocale.gui_multiplayerWorld_loggedIn,
					game.colorScheme.text_good);
				},
				disconnect: function(player) {
					if (player in this.list) {
						this.list[player].currentGuiType = "default";
						
						game.realms.gameWorld.objects[[
							this.list[player].position.x,
							this.list[player].position.y
						]] = new game.realms.gameWorld.objectTypes.grave.New({
							name: player
						});
						
						delete this.list[player];
						
						console.log(player + " logged out");
						game.realms.gameWorld.guiTypes.default.writeToChat("",
							player + game.currentLocale.gui_multiplayerWorld_loggedOut,
						game.colorScheme.text_bad);
					}
				}
		},
	
	//Methods
		init: function() {
			function loadScript(path) {
				try {
					new Function(
						"require",
						"nodeCrypto",
						"fs",
						"zlib",
						"url",
						"path",
						"net",
						
						fs.readFileSync(path, "utf-8")
					)(
						require,
						crypto,
						fs,
						zlib,
						url,
						path,
						net
					);
				} catch (error) {
					console.log(new ReferenceError(
						"can't load " + path +
						
						" (" + error + ")"
					));
				}
			}
			
			loadScript("client/main.js");
			loadScript("client/main_loadScripts.js");
			loadScript("client/main_loadTexture.js");
			loadScript("client/main_loadSound.js");
			loadScript("client/config.js");
			loadScript("client/input.js");
			loadScript("client/input_keyboard.js");
			loadScript("client/input_mouse.js");
			loadScript("client/input_navigation.js");
			loadScript("client/mods.js");
			
			game.loadScripts();
			
			game.config.read();
			
			//Generate spawn cluster
				if (!game.config.list.gameWorld.version) {
					game.config.list.gameWorld.version = game.version;
					
					game.realms.gameWorld.generator.generateSpawnCluster();
					
					game.realms.gameWorld.playerObject = null;
					
					game.config.write("gameWorld");
				}
			
			//Generate default GUI data
				for (var i in game.realms.gameWorld.guiTypes) {
					this.players.defaultGuiData[i] =
						game.realms.gameWorld.guiTypes[i].defaultData;
				}
			
			this.updater = setInterval(function() {
				app.virtualClient.update();
			}, 40);
		},
		
		handleInput: function(player, data) {
			this.players.setCurrent(player);
			
			switch (data.type) {
				case "keyboard":
					game.input.keyboard.catch(String(data.keyCode), ({
						press: "press",
						release: "release"
					})[data.action] || "press");
					
					break;
				case "mouse":
					game.input.mouse.catch(
						parseInt(data.x) || -1,
						parseInt(data.y) || -1,
						
						({
							press: "press",
							movement: "movement",
							release: "release"
						})[data.action] || "press"
					);
					
					break;
				case "navigation":
					game.input.navigation.catch();
					
					break;
			}
			
			game.realms.gameWorld.currentGuiType.handleInput();
			
			game.input.flush();
		},
		
		handleDialog: function(player, dialogType, result) {
			this.players.setCurrent(player);
			
			switch (dialogType) {
				case "alert":
					if (app.sessions[player].data.alert.handler) {
						app.sessions[player].data.alert.handler(result);
						
						delete app.sessions[player].data.alert.handler;
					}
					
					break;
				case "prompt":
					if (app.sessions[player].data.prompt.handler) {
						app.sessions[player].data.prompt.handler(result);
						
						delete app.sessions[player].data.prompt.handler;
					}
					
					break;
				case "confirm":
					if (app.sessions[player].data.confirm.handler) {
						app.sessions[player].data.confirm.handler(result);
						
						delete app.sessions[player].data.confirm.handler;
					}
					
					break;
			}
		},
		
		update: function() {
			for (var i in this.players.list) {
				this.players.setCurrent(i);
				
				game.realms.gameWorld.updateChunk();
				
				if (!this.players.list[i]) {
					continue;
				}
				
				var renderingData = {
					gfx: game.realms.gameWorld.update(),
					
					debugInfo: {
						software:
							app.manifest.name +
							" (" + game.name + " " + game.version + ") " +
							app.manifest.version,
						
						cameraPosition: game.config.list.gameWorld.cameraPosition,
						playerPosition: game.config.list.gameWorld.playerPosition
					}
				};
				
				if (app.sessions[i].data.buffer != JSON.stringify(renderingData)) {
					app.sessions[i].data.buffer = JSON.stringify(renderingData);
					
					app.sessions[i].reply("rendering", renderingData);
				}
			}
			
			game.realms.gameWorld.updatedAreas.length = 0;
			
			if (game.realms.gameWorld.tick < 24) {
				game.realms.gameWorld.tick++;
			} else {
				game.realms.gameWorld.tick = 0;
			}
		}
};
